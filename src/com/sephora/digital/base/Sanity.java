package com.sephora.digital.base;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.io.IOException;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Sanity {
	    public WebDriver driver= null;
	    
	    @BeforeClass
	    public void setUp() throws Exception {
		//closeSimulatorAndInstruments();
		//Process appium = Runtime.getRuntime().exec("appium &");
		Thread.sleep(5000);
		
		File app = new File("/Users/Shared/Jenkins/Home/jobs/ciqbuild/workspace/build/Sephora.app");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability(CapabilityType.PLATFORM, "Mac");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("deviceName", "iPad 2");	
		capabilities.setCapability("newCommandTimeout", "800");
		capabilities.setCapability("noReset", "true");
		
		//Create an instance of RemoteWebDriver and connect to the Appium server.
		driver = new RemoteWebDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
		//Thread.sleep(1000);				
	    }
	    
	    @Test
	    public void DataSynchTime() throws Exception {
	    	Calendar cal = Calendar.getInstance();
	    	cal.getTime();
	    	SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
	    	System.out.println( sdf1.format(cal.getTime()) );	
			long startTime = System.nanoTime();			
	        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[2]")));
	        long endTime = System.nanoTime();     
			Calendar cal1 = Calendar.getInstance();
	    	cal1.getTime();
	    	SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
	    	System.out.println( sdf2.format(cal1.getTime()) );						
	        System.out.println("Took "+(endTime - startTime)/(1000000000) + " seconds");	    	
	    }
	    
		@Test
		public void SearchByFoundation() throws Exception {       
        //Search by Foundation
		new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[2]")));
		driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[2]")).click();
        Thread.sleep(1000);   
        //Select Brand
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAImage[1]")).click();
        Thread.sleep(1000); 
        //Select Product
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[1]")).click();
        Thread.sleep(1000);
        //Select Tile
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[1]/UIAStaticText[1]")).click();
        Thread.sleep(1000);
        //Click Email and Save
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[11]")).click();
        //Enter Email id        
        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")));
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")).click();       
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")).sendKeys("Naveen.Kesari@sephora.com");
        //Click Send
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[9]")).click();     
        //Click Close
        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[4]")));
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[4]")).click();
        //Navigate to Home
        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]")));
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]")).click();                 
		}
		
		@Test
		public void SearchBySkintone() throws Exception {
	        //Type Skintone
	        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAImage[3]")));
			driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAImage[3]")).click();
	        Thread.sleep(1000);   
	        //Enter Skintone
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[2]")).click();
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[15]")).click();
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[14]")).click();
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[2]")).click();
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[9]")).click();	      
	        //Enter Email id
	        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")));
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")).click();	        
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")).sendKeys("Naveen.Kesari@sephora.com");	               
	        //Click Send
	        driver.findElement(By.name("Send")).click();
	        //Click Close
	        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[4]")));
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[4]")).click();
	        Thread.sleep(1000);	        	                
	        //Click Email and Save
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[11]")).click();
	        Thread.sleep(1000);
	        //Enter Email id        
	        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")));
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")).click();	        
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIATextField[3]")).sendKeys("Naveen.Kesari@sephora.com");	        
	        //Click Send
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[9]")).click();
	        //Click Close
	        new WebDriverWait(driver,800).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[4]")));
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAPopover[1]/UIAButton[4]")).click();
	        Thread.sleep(1000);
	        //Navigate to Home
	        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]")).click();
	        Thread.sleep(1000);
			}
		
		@AfterClass
		public void tearDown() throws Exception {
		//Close the app and simulator
		driver.quit();
		//appium.destroy();
		}
		}



	

